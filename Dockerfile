FROM node:20-alpine

WORKDIR /app

COPY . .

RUN npm install 

RUN npm install -g pm2 

EXPOSE 3000

CMD ["npm","start"]
